<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PostsController@index')->name('home');
Route::get('/posts/create', 'PostsController@create');
Route::post('/posts', 'PostsController@store');

Route::get('user/{user}/posts/{post}/{title}','PostsController@show');
Route::get('/user/{user}','PostsController@usersPost');
Route::post('/posts/{post}/comments','CommentsController@store');

Route::get('/category/{category}/{title}','PostsController@category');
Route::get('/user/{user}/category/{category}/{title}','PostsController@usersCategoryPost');


Route::get('/register','RegistrationController@create');
Route::post('/register','RegistrationController@store');

Route::get('/login','SessionsController@create');
Route::post('/login','SessionsController@store');
Route::get('/logout','SessionsController@destroy');

//edit
Route::get('/post/{id}/edit', 'PostsController@edit');

Route::patch('/post/update/{id}', 'PostsController@update');

Route::get('/post/{id}/delete', 'PostsController@destroy');