About Photo Blog
======


How to Install
======
````
1. `git clone [url]`
2. `composer update`
3. `php artisan vendor:publish --provider="Intervention\Image\ImageServiceProviderLaravel5`
4. `php artisan vendor:publish --tag=ckeditor`
5. `composer dump-autoload`
````

Create your own .env file into the folder where the project is created

````
1. `php artisan migrate`
2. `php artisan db:seed`
````

[optionally]


- php artisan serve [only for development purpose]
- Browse the url which is showd on CLI
