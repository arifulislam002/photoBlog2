@extends('layouts.app')

@section('content')
    @include('layouts.nav')

    <div class="container">

        <div class="col-sm-8 blog-main">

            <h1>Register</h1>
            <hr>
            {!! Form::open(['url' => 'register', 'method'=>'post', 'files' => true,'role'=>'form']) !!}

                <div class="form-group">
                    {!! Form::label('name','Name') !!}
                    {!! Form::text('name',null,['class'=>'form-control','id'=>'name','required' ]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email','email') !!}
                    {!! Form::email('email',null,['class'=>'form-control','id'=>'email','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('picture','Profile Picture') !!}
                    {!! Form::file('picture', ['accept' => 'image/*','id'=>'picture','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('cover_photo','Cover Photo') !!}
                    {!! Form::file('cover_photo', ['accept' => 'image/*','id'=>'cover_photo','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password','Password') !!}
                    {!! Form::password('password',['class'=>'form-control','id'=>'password','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password_confirmation','Password') !!}
                    {!! Form::password('password_confirmation',['class'=>'form-control','id'=>'password_confirmation','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Register',['class'=>'btn btn-primary']) !!}
                </div>

                <div class="form-group">
                    @include('layouts.errors')
                </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
