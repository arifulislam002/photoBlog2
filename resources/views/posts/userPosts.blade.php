@extends('layouts.app')

@section('content')

    @include('layouts.userHeader')

    <hr>
    <div class="container">

        <div class="row">
            <ul class="nav">

                <li class="nav-item">
                    <a href="{{ url('user/'.$user->id)}}" class="nav-link {{ (Request::segment(3) == '' ? 'active-nav' : ' ') }}" >Home</a>
                </li>


                @foreach($categories as $category)

                    <li class="nav-item">
                        <a class="nav-link {{ (Request::segment(5) == make_slug(strtolower($category->title)) ? 'active-nav' : ' ') }}"  href="{{ url('user/'.$user->id.'/category/'.$category->id.'/'.make_slug($category->title)) }}">{{$category->title}}</a>
                    </li>

                @endforeach
            </ul>

        </div>

    </div>
    <hr>

    @include('posts.post')

@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('front-end/css/custom.css') }}">
     <style>
        .active-nav{
            background: #e4edff;
        }
    </style>
@endpush