@extends('layouts.app')


@section('content')
    <!-- Navigation -->
    @include('layouts.tap')

      <!-- Page Features -->
    @if($flash =  session('msg'))
    {{ $flash }}
    @endif

    <div class="container">

          @include('posts.post')

    </div>
      <!-- /.row -->
@endsection