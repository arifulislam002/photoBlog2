@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 blog-main">
                <h1>Publish a post</h1>
                <hr>

                {!! Form::open(['url' => 'post/update/'.$post->id, 'method'=>'patch', 'files' => true,'role'=>'form']) !!}

                <div class="form-group">
                    {!! Form::label('category_id', 'Category') !!}
                    {!! Form::select('category_id', $categories, $post->category->id, ['placeholder' => 'Select One ...', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title',$post->title,['class'=>'form-control', 'required'=>'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('body','Body') !!}
                    {!! Form::textarea('body',$post->body,['class'=>'form-control','id'=>'post-ckeditor']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('image','image') !!}
                    <img class="card-img-top" id="blah"  src="{{ asset('uploads/post-image/'.$post->image) }}" alt="">

                    {!! Form::file('image', ['accept' => 'image/*','id'=>'imgInp']) !!}
                </div>

                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
                <br>
                @include('layouts.errors')

            </div>
        </div>
    </div>
    <br>
@endsection

@push('scripts')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'post-ckeditor' );
</script>
<script>

function readURL(input) {

if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onload = function (e) {
$('#blah').attr('src', e.target.result);
}

reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp").change(function(){
readURL(this);
});
</script>

@endpush
