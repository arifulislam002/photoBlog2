<div class="container">
    <div class="row">
        @foreach($posts as $post)

            <div class="col-lg-3 col-md-6 mb-4">
                <div class="card">
                    <a href="{{ url('user/'.$post->user->id.'/posts/'.$post->id.'/'.$post->title) }}">
                        <img class="card-img-top" src="{{ asset('uploads/post-image/'.$post->image) }}" alt="">
                    </a>
                    <div class="card-body">
                        <time class="pull-left">{{ $post->created_at->toFormattedDateString() }}</time>
                        <a href="{{ url('user/'.$post->user->id.'/posts/'.$post->id.'/'.make_slug(str_limit($post->title,15))) }}"><h4 class="card-title">{!!  str_limit($post->title,30)  !!}</h4></a>
                        <p class="card-text">{!! str_limit($post->body,150) !!}</p>
                    </div>
                    <div class="card-footer">
                         by <a href="{{ url('user/'.$post->user->id) }}"> {{$post->user->name}}</a>
                    </div>
                    <p>
                    @if(Auth::check() && auth()->user()->id==$post->user->id)
                    <a href="{{ url('post/'.$post->id.'/edit')}}" class="btn btn-primary">Edit</a>
                    <a href="{{ url('post/'.$post->id.'/delete')}}" class="btn btn-primary">Delete</a>
                        @endif
                    </p>
                    <a href="#" class="btn btn-info">Like</a>
                    <a href="" class="btn btn-info">Dislike</a>
                </div>

            </div>

        @endforeach
    </div>

    {{ $posts->links('layouts.pagination') }}

</div>