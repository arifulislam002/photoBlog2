@extends('layouts.app')

@section('content')
    @include('layouts.userHeader')
    {{--{{ $user->profile->picture }}--}}

    <!-- Navigation -->
    <hr>
    <div class="container">

        <div class="row">
            <ul class="nav">

              <li class="nav-item">
                <a href="{{ url('user/'.$user->id)}}" class="nav-link {{ (Request::segment(3) == '' ? 'active-nav' : ' ') }}" >Home</a>
             </li>
             
                @foreach($categories as $category)

                    <li class="nav-item">
                        <a class="nav-link {{ (Request::segment(3) == make_slug(strtolower($category->title)) ? 'active-nav' : ' ') }}"  href="{{ url('user/'.$user->id.'/category/'.$category->id.'/'.make_slug($category->title)) }}">{{$category->title}}</a>
                    </li>

                @endforeach
            </ul>

        </div>

    </div>
    <hr>

    <div class="container">
        <div class="col-sm-12 blog-main">
            <h2 class="blog-post-title">{{ $post->title }}</h2>
            <p class="blog-post-meta">{{ $post->created_at->toFormattedDateString() }}</p>
            {!! $post->body !!}<br><br>
            <img src="{{ asset('uploads/post-image/'.$post->image) }}" style="width: 100%">
            <hr>
            @if(Auth::check())
                <div class="comments">
                    <ul class="list-group">
                        @foreach($post->comments as $comment)
                            <li class="list-group-item">
                                <strong>
                                    {{ $comment->created_at->diffForHumans() }}:&nbsp;
                                </strong>
                                {{ $comment->body }} by <mark>{{$comment->user->name}}</mark>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="card">
                    <div class="card-block">
                        <form method="POST" action="/posts/{{$post->id}}/comments">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <textarea name="body" class="form-control" placeholder="Your comment here..." required></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" >Add Comment</button>
                            </div>
                        </form>
                        @include('layouts.errors')
                    </div>
                </div>
            @else
               <a href="{{url('login')}}" class="btn btn-primary btn-lg">Login to comment</a><hr>
            @endif
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('front-end/css/custom.css') }}">

    <style>
        .active-nav{
            background: #e4edff;
        }
    </style>
@endpush