@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 blog-main">
                <h1>Publish a post</h1>
                <hr>

                {!! Form::open(['url' => 'posts', 'method'=>'post', 'files' => true,'role'=>'form']) !!}

                    <div class="form-group">
                        {!! Form::label('category_id', 'Category') !!}
                        {!! Form::select('category_id', $categories, null, ['placeholder' => 'Select One ...', 'class' => 'form-control', 'required' => 'required']) !!}
                        {{--{!! Form::select('category_id', $categories,isset($program)?$program->categories()->pluck('category_id')->toArray():null, ['class' => 'full-width select2-hidden-accessible select2-search__field','aria-hidden'=>"true", 'data-init-plugin'=>"select2",'tabindex'=>"-1",'selected'=>"selected" ,'multiple' => 'true','id' => 'categories','required'=>'required']) !!}--}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title', 'Title') !!}
                        {!! Form::text('title',null,['class'=>'form-control', 'required'=>'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('body','Body') !!}
                        {!! Form::textarea('body',null,['class'=>'form-control','id'=>'post-ckeditor']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('image','image') !!}
                        {!! Form::file('image', ['accept' => 'image/*']) !!}
                    </div>

                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
                <br>
                @include('layouts.errors')

            </div>
        </div>
    </div>
    <br>
@endsection

@push('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'post-ckeditor' );
    </script>
@endpush
