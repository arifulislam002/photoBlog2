<hr>
<div class="container">

    <div class="row">

        <ul class="nav">

          <li class="nav-item">
            <a href="{{ url('/')}}" class="nav-link {{ (Request::segment(1) == '' ? 'active-nav' : ' ') }}" >Home</a>
         </li>

            @foreach($categories as $category)

                <li class="nav-item {{ (Request::segment(3) == make_slug(strtolower($category->title)) ? 'active-nav' : ' ') }}">
                    <a class="nav-link"  href="{{ url('category/'.$category->id.'/'.make_slug($category->title)) }}">{{$category->title}}</a>
                </li>

            @endforeach
        </ul>

    </div>

</div>
<hr>

@push('css')
    <style>
        .active-nav{
            background: #e4edff;
        }
    </style>
@endpush