<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">Photo Blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <!-- <li class="nav-item active">
                    <a class="nav-link" href="#">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li> -->
                @if(Auth::check())
                    &nbsp;
                    <div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">{{ Auth::user()->name }}
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('posts/create') }}">Create Post</a></li>
                            <li><a href="{{ url('logout') }}">Logout</a></li>
                        </ul>
                    </div>
                @else  &nbsp;
                <a class="nav-link ml-auto bg-primary" href="{{ url('register') }}">Register</a>&nbsp;
                <a class="nav-link ml-auto bg-success" href="{{ url('login') }}">Login</a>
                @endif
            </ul>
        </div>
    </div>
</nav>