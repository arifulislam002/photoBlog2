<div class="user-profile">
    <img align="left" class="user-image-lg" src="{{ asset('uploads/user-cover-photo/'.$user->profile->cover_photo) }}" alt="Profile image example"/>
    <img align="left" class="user-image-profile img-circle" src="{{ asset('uploads/user-picture/'.$user->profile->picture) }}" alt="Profile image example"/>
    <div class="user-profile-text">
        <h1>{{ $user->name }}</h1>
    </div>
</div>
<div class="clearfix"></div>
