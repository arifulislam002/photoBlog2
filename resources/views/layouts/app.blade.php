<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Photo Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('front-end/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('front-end/css/heroic-features.css')}}" rel="stylesheet">

    {{--page specific style--}}
    @stack('css')

  </head>

  <body>

    @include('layouts.nav')

    <!-- Page Content -->

      @yield('content')


    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('front-end/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('front-end/vendor/popper/popper.min.js') }}"></script>
    <script src="{{ asset('front-end/vendor/bootstrap/js/bootstrap.min.js') }}"></script>



    {{--page specific scripts--}}
    @stack('scripts')

  </body>

</html>
