<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['category_id','user_id','title','body','image'];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function addComment($body)
    {
//        Comment::create([
//            'body'=>$body,
//            'post_id'=>$this->id
//        ]);
      //  $this->comments()->create(compact('body'));
        $data['body'] =$body;
        $data['user_id'] =auth()->id();

        $this->comments()->create($data);

    }

    public function user()
    {
        return $this->belongsTo('App\user');
    }



}
