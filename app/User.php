<?php

namespace App;

use App\Http\Helpers\Imager;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function publish(Post $post)
    {
        //        Post::create([
        //            'title' => request('title'),
        //            'body' => request('body'),
        //            'user_id' => auth()->id()
        //        ]);
        $this->posts()->save($post);
    }

    public static function picture()
    {

        $imager = new Imager('user-picture',['size' => [265, 265]]);
        $file = request()->file('picture');
        return $imager->upload($file,request('name'));
    }

    public static function coverPhoto()
    {
        $imager = new Imager('user-cover-photo',['size' =>[1400, 370]]);
        $file = request()->file('cover_photo');
        return $imager->upload($file,request('name'));
    }


}
