<?php
function make_slug($string,$symbol='-') {

    return preg_replace('/\s+/u', $symbol, trim(strtolower($string)));
}
