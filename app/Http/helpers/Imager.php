<?php
namespace App\Http\Helpers;
use Image;
use Carbon\Carbon;
use Storage;

class Imager
{
    private $uploadBasePath;
    private $subDirectory;

    public function __construct($subDirectory,$conf = null)
    {
        $this->uploadBasePath = public_path('uploads/');
        $this->subDirectory = $subDirectory;
        if(!is_null($conf))
        {
            $this->conf = $conf;
        }
    }

    public function upload($file,$title)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $image_file_name = $timestamp.'-'.$title. '.' . $file->getClientOriginalExtension();
        foreach ($this->conf as $size) {
            Image::make($file)->resize($size[0], $size[1])->save($this->uploadBasePath . $this->subDirectory  ."/". $image_file_name);
        }
        return $image_file_name;
    }

}