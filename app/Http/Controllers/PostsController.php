<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use App\Category;
use App\User;
use Carbon\Carbon;
use Image;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    const UPLOAD_DIR = '/uploads/post-image/';

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show','usersPost','category','usersCategoryPost']);
    }

    public function index()
    {
        $posts = Post::latest()->paginate(12);
        $categories = Category::all();
        return view('posts.index',compact('posts','categories'));
    }

    public function category(Category $category)
    {
        $posts = $category->posts()->paginate(12);
        $categories = Category::all();

        return view('posts.index',compact('posts','categories'));
    }

    public function usersPost(User $user)
    {
        $posts = $user->posts()->paginate(12);

        $categories = [];

        foreach($posts as $p)
        {
            $categories[] =  $p->category;
        }

        $categories = array_unique($categories);

        return view('posts.userPosts',compact('posts','user','categories'));
    }

    public function usersCategoryPost(User $user,Category $category)
    {

        $posts = Post::latest()->where('user_id',$user->id)->where('category_id',$category->id)->paginate(16);

        $uPosts = Post::latest()->where('user_id',$user->id)->get();
        
        $categories = [];

        foreach($uPosts as $p)
        {
            $categories[] =  $p->category;
        }

        $categories = array_unique($categories);

        return view('posts.userPosts',compact('posts','user','categories'));

    }

    public function create()
    {
        $categories = Category::orderBy('title')->pluck('title', 'id');
        return view('posts.create',compact('categories'));
    }

    public function store(PostRequest $request)
    {

        $data = $request->only(['category_id','title', 'body']);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            //dd($file);
            $data['image'] = $this->uploadImage($file);
        } else {
            $data['image'] = null;
        }


        $data['user_id'] =auth()->id();
        Post::create($data);
        return redirect('/');
    }

    public function show(User $user,Post $post)
    {

        $posts = $user->posts()->get();

        $categories = [];

        foreach($posts as $p)
        {
            $categories[] =  $p->category;
        }

        $categories = array_unique($categories);

        return view('posts.show',compact('post','user','categories'));
    }


    private function uploadImage($file)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $image_file_name = $timestamp . '.' . $file->getClientOriginalExtension();
        Image::make($file)->save(public_path() . self::UPLOAD_DIR . $image_file_name);
        //$file->move(public_path() . self::UPLOAD_DIR, $image_file_name);
        return $image_file_name;
    }

    public function edit($id){


        $categories = Category::pluck('title','id');

        $post = Post::find($id);
        $user_id=  auth()->user()->id;
        $post_id=$post->user_id;
        if($user_id==$post_id){
            return view('posts.edit',compact('post','categories'));

        }
        else{
session()->flash('msg','You have no access');
            return redirect('/');

        }

    }



    public function update(Request $request,$id){
        $postInfo=Post::find($id);

      $data['title']=$request->title;
      $data['category_id']=$request->category_id;
      $data['body']=$request->body;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            //dd($file);
            $data['image'] = $this->uploadImage($file);
        } else {
            $data['image'] = null;
        }

        $postInfo->update($data);

        return redirect('/');
    }

    public function destroy($id)
    {
        $post_comments = Post::find($id)->comments->count();
        if ($post_comments > 0) {
            session()->flash('msg',"You  have comments, so you can't delete this post");
            return redirect('/');
        } else {
            Post::find($id)->delete();
            return redirect('/');

        }
    }
}
