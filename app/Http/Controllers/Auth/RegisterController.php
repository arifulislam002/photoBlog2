<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Image;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        $data['name'] = request('name');
        $data['email'] = request('email');
        $data['password'] =bcrypt(request('password')) ;
        $user = User::create($data);

        if ($request->hasFile('picture')) {
            $profileData['picture'] = User::picture();
        }

        if ($request->hasFile('cover_photo')) {
            $profileData['cover_photo'] = User::coverPhoto();
        }

        $user->profile()->create($profileData);

        auth()->login($user);

        return redirect()->home();
    }

}
