-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 17, 2017 at 02:02 PM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `photoblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Nature', '2017-09-17 07:15:28', '2017-09-17 07:15:28'),
(2, 'Sports', '2017-09-17 07:15:28', '2017-09-17 07:15:28'),
(3, 'Life Style', '2017-09-17 07:15:28', '2017-09-17 07:15:28'),
(4, 'Travel', '2017-09-17 07:15:28', '2017-09-17 07:15:28'),
(5, 'Festival', '2017-09-17 07:15:28', '2017-09-17 07:15:28');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'nice', '2017-09-17 07:23:02', '2017-09-17 07:23:02'),
(2, 6, 1, 'good', '2017-09-17 07:23:16', '2017-09-17 07:23:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_09_14_093910_create_posts_table', 1),
(4, '2017_09_14_152146_create_comments_table', 1),
(5, '2017_09_14_175624_create_profiles_table', 1),
(6, '2017_09_15_191039_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `category_id`, `title`, `body`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'Rerum quia illum laboriosam quia vel.', 'Father William,\' the young Crab, a little bit of the house down!\' said the Queen was silent. The King laid his hand upon her arm, with its eyelids, so he did,\' said the Dodo, \'the best way to change the subject. \'Ten hours the first to break the silence. \'What day of the table. \'Nothing can be clearer than THAT. Then again--\"BEFORE SHE HAD THIS FIT--\" you never even introduced to a lobster--\'.', 'da8d6d68ef18355780afac5c49ec373e.jpg', '2017-09-17 07:15:30', '2017-09-17 07:15:30'),
(2, 1, 5, 'Occaecati sint autem perspiciatis quidem ipsam ea.', 'While she was shrinking rapidly; so she went on. \'We had the best plan.\' It sounded an excellent opportunity for making her escape; so she sat on, with closed eyes, and half of anger, and tried to say which), and they repeated their arguments to her, one on each side to guard him; and near the house of the wood--(she considered him to you, Though they were nowhere to be sure, this generally.', 'ce84746ac5a1c45cfc773bdf2685468c.jpg', '2017-09-17 07:15:32', '2017-09-17 07:15:32'),
(3, 1, 2, 'Quasi quisquam et vero eligendi similique.', 'Queen said severely \'Who is this?\' She said the Queen. \'It proves nothing of the others looked round also, and all dripping wet, cross, and uncomfortable. The first witness was the cat.) \'I hope they\'ll remember her saucer of milk at tea-time. Dinah my dear! I wish I hadn\'t begun my tea--not above a week or so--and what with the clock. For instance, if you were down here with me! There are no.', 'db86ca4c1f4e08df445d8ce52c8037ac.jpg', '2017-09-17 07:15:33', '2017-09-17 07:15:33'),
(4, 1, 4, 'Ducimus totam dolor velit nisi laboriosam aliquam deleniti.', 'HERE.\' \'But then,\' thought Alice, \'they\'re sure to happen,\' she said to herself, \'Why, they\'re only a mouse that had fallen into the garden, where Alice could bear: she got to see it again, but it all came different!\' Alice replied very politely, \'if I had our Dinah here, I know THAT well enough; and what does it matter to me whether you\'re a little pattering of feet on the same thing as a boon.', '934857debd91d432c19b402feeb9194e.jpg', '2017-09-17 07:15:34', '2017-09-17 07:15:34'),
(5, 1, 5, 'Sed quidem reprehenderit sit.', 'Alice, that she let the Dormouse again, so that her flamingo was gone in a very small cake, on which the March Hare will be When they take us up and walking away. \'You insult me by talking such nonsense!\' \'I didn\'t mean it!\' pleaded poor Alice. \'But you\'re so easily offended, you know!\' The Mouse did not much surprised at this, she looked down, was an old Crab took the hookah out of breath, and.', 'e6e9e42cc562a568bde0a2b78c0fa264.jpg', '2017-09-17 07:15:36', '2017-09-17 07:15:36'),
(6, 2, 4, 'Suscipit quis et ea quod laborum odit nihil consequatur.', 'Queen furiously, throwing an inkstand at the beginning,\' the King said to herself in a piteous tone. And she began thinking over other children she knew that were of the Mock Turtle\'s Story \'You can\'t think how glad I am now? That\'ll be a footman because he was obliged to have been a RED rose-tree, and we put a white one in by mistake; and if it likes.\' \'I\'d rather not,\' the Cat again, sitting.', '9309c515283b445db8d6e89be4253f87.jpg', '2017-09-17 07:15:38', '2017-09-17 07:15:38'),
(7, 2, 4, 'Sed et sequi adipisci quas.', 'France-- Then turn not pale, beloved snail, but come and join the dance? \"You can really have no answers.\' \'If you knew Time as well she might, what a long breath, and till the eyes appeared, and then quietly marched off after the candle is like after the rest waited in silence. Alice noticed with some severity; \'it\'s very rude.\' The Hatter was the first figure!\' said the Caterpillar seemed to.', 'a6eba945bcbcb0ab7aeec963650b4237.jpg', '2017-09-17 07:15:39', '2017-09-17 07:15:39'),
(8, 2, 1, 'Occaecati perspiciatis saepe quasi est sunt tempore.', 'Rabbit actually TOOK A WATCH OUT OF ITS WAISTCOAT-POCKET, and looked anxiously round, to make ONE respectable person!\' Soon her eye fell on a three-legged stool in the distance. \'Come on!\' and ran the faster, while more and more puzzled, but she saw in my size; and as Alice could not help bursting out laughing: and when she caught it, and they all quarrel so dreadfully one can\'t hear oneself.', '9c27e5048a5eb961e5930dd634b45774.jpg', '2017-09-17 07:15:41', '2017-09-17 07:15:41'),
(9, 2, 3, 'Porro numquam molestias consequatur quos.', 'Mock Turtle replied; \'and then the puppy jumped into the way to change the subject. \'Go on with the grin, which remained some time without hearing anything more: at last turned sulky, and would only say, \'I am older than you, and don\'t speak a word till I\'ve finished.\' So they went on again:-- \'I didn\'t know that cats COULD grin.\' \'They all can,\' said the Dormouse; \'VERY ill.\' Alice tried to say.', '58af80cacc298797e957392d23d18f78.jpg', '2017-09-17 07:15:44', '2017-09-17 07:15:44'),
(10, 2, 2, 'Distinctio non tenetur et.', 'Alice waited patiently until it chose to speak first, \'why your cat grins like that?\' \'It\'s a friend of mine--a Cheshire Cat,\' said Alice: \'allow me to sell you a song?\' \'Oh, a song, please, if the Mock Turtle sighed deeply, and drew the back of one flapper across his eyes. \'I wasn\'t asleep,\' he said in a great hurry. \'You did!\' said the Eaglet. \'I don\'t know what \"it\" means.\' \'I know SOMETHING.', '2ef674565afbee259e78148f4eb9389c.jpg', '2017-09-17 07:15:45', '2017-09-17 07:15:45'),
(11, 3, 5, 'Placeat aut consequatur quis aperiam aliquam nesciunt facilis repellat.', 'While the Duchess and the executioner went off like an honest man.\' There was a different person then.\' \'Explain all that,\' he said in a very interesting dance to watch,\' said Alice, \'and why it is all the things I used to say.\' \'So he did, so he did,\' said the Dodo. Then they both bowed low, and their curls got entangled together. Alice laughed so much frightened to say whether the pleasure of.', 'd70e9b16539af135edd7e49c1bc591c4.jpg', '2017-09-17 07:15:47', '2017-09-17 07:15:47'),
(12, 3, 5, 'Asperiores eius nisi illo dignissimos tempore nihil numquam.', 'PLEASE mind what you\'re at!\" You know the meaning of it in her brother\'s Latin Grammar, \'A mouse--of a mouse--to a mouse--a mouse--O mouse!\') The Mouse did not get dry again: they had been to the cur, \"Such a trial, dear Sir, With no jury or judge, would be so easily offended, you know!\' The Mouse looked at Alice, and she felt that it felt quite relieved to see what was coming. It was high time.', '750cde2910338d5914fa50fd1a651c6d.jpg', '2017-09-17 07:15:49', '2017-09-17 07:15:49'),
(13, 3, 1, 'Ducimus culpa excepturi dolore numquam.', 'For, you see, as she went on. \'Would you like to be full of tears, but said nothing. \'This here young lady,\' said the Gryphon interrupted in a helpless sort of way, \'Do cats eat bats, I wonder?\' As she said aloud. \'I shall do nothing of the cupboards as she came up to the Duchess: \'flamingoes and mustard both bite. And the executioner went off like an honest man.\' There was a large mustard-mine.', '6ba2acff63160cc46c659deec1721e03.jpg', '2017-09-17 07:15:50', '2017-09-17 07:15:50'),
(14, 3, 3, 'Itaque ex ipsum inventore pariatur vel ex asperiores.', 'Alice was beginning to feel a little quicker. \'What a curious feeling!\' said Alice; \'all I know who I WAS when I was thinking I should think you\'ll feel it a bit, if you don\'t know what to do, and perhaps after all it might appear to others that what you would seem to put everything upon Bill! I wouldn\'t be so easily offended!\' \'You\'ll get used up.\' \'But what did the Dormouse sulkily remarked.', 'd32d2a0898bc3877799260a37f7c6003.jpg', '2017-09-17 07:15:52', '2017-09-17 07:15:52'),
(15, 3, 5, 'Sequi ullam maiores necessitatibus est odio et.', 'His voice has a timid and tremulous sound.] \'That\'s different from what I see\"!\' \'You might just as if nothing had happened. \'How am I then? Tell me that first, and then, if I chose,\' the Duchess asked, with another hedgehog, which seemed to have any rules in particular; at least, if there are, nobody attends to them--and you\'ve no idea what to do, so Alice soon began talking again. \'Dinah\'ll.', '5c1bfdeed1c5bb02fd9496c91edd6f38.jpg', '2017-09-17 07:15:54', '2017-09-17 07:15:54'),
(16, 4, 4, 'Laborum ratione illum aut quas.', 'I can\'t quite follow it as you liked.\' \'Is that the Queen till she was up to her head, she tried to speak, but for a minute, while Alice thought she might as well as she could, for the garden!\' and she jumped up on tiptoe, and peeped over the wig, (look at the end of the shelves as she couldn\'t answer either question, it didn\'t sound at all comfortable, and it put the Dormouse fell asleep.', 'b8e0d19852e02603de99cf489d744d6e.jpg', '2017-09-17 07:15:55', '2017-09-17 07:15:55'),
(17, 4, 5, 'Adipisci ut voluptatibus quia tenetur voluptas.', 'Hatter. \'It isn\'t mine,\' said the youth, \'and your jaws are too weak For anything tougher than suet; Yet you balanced an eel on the table. \'Have some wine,\' the March Hare will be the right size again; and the game was in March.\' As she said to the heads of the court,\" and I shall be punished for it now, I suppose, by being drowned in my size; and as Alice could think of nothing else to say when.', 'd07439c278ed03230c8ddcd8f269858a.jpg', '2017-09-17 07:15:57', '2017-09-17 07:15:57'),
(18, 4, 5, 'Tempore quae quibusdam quos et rem.', 'And will talk in contemptuous tones of the guinea-pigs cheered, and was just beginning to think that very few things indeed were really impossible. There seemed to be no sort of mixed flavour of cherry-tart, custard, pine-apple, roast turkey, toffee, and hot buttered toast,) she very seldom followed it), and sometimes shorter, until she made out that one of the players to be afraid of them!\'.', 'a21da97aa0878b0a4714423d2dc3a33a.jpg', '2017-09-17 07:15:58', '2017-09-17 07:15:58'),
(19, 4, 2, 'Dicta quod aut molestiae.', 'It sounded an excellent plan, no doubt, and very nearly getting up and repeat \"\'TIS THE VOICE OF THE SLUGGARD,\"\' said the Mock Turtle drew a long tail, certainly,\' said Alice, \'how am I to do this, so that they must needs come wriggling down from the Queen said to Alice, she went on growing, and, as the doubled-up soldiers were always getting up and to stand on your head-- Do you think you can.', '83ef7674b0ab2f6508ea4e271e29d2a7.jpg', '2017-09-17 07:16:00', '2017-09-17 07:16:00'),
(20, 4, 5, 'Qui libero molestiae quas.', 'ONE respectable person!\' Soon her eye fell on a crimson velvet cushion; and, last of all her coaxing. Hardly knowing what she did, she picked her way out. \'I shall sit here,\' he said, \'on and off, for days and days.\' \'But what happens when one eats cake, but Alice had not gone far before they saw her, they hurried back to the Knave of Hearts, carrying the King\'s crown on a little way off, and.', 'f17a0d36e1a231426ab8739b7465fe10.jpg', '2017-09-17 07:16:01', '2017-09-17 07:16:01'),
(21, 5, 3, 'Temporibus hic hic laborum odit qui nobis ullam.', 'Majesty!\' the soldiers had to fall a long time together.\' \'Which is just the case with MINE,\' said the Dormouse, without considering at all this grand procession, came THE KING AND QUEEN OF HEARTS. Alice was beginning to grow up any more questions about it, even if I might venture to ask help of any one; so, when the tide rises and sharks are around, His voice has a timid and tremulous sound.].', '52a7f682eb319ec95dfd67f5535a4d98.jpg', '2017-09-17 07:16:03', '2017-09-17 07:16:03'),
(22, 5, 5, 'Quaerat aliquid est sunt beatae quibusdam.', 'Waiting in a moment. \'Let\'s go on with the name again!\' \'I won\'t have any pepper in my kitchen AT ALL. Soup does very well to introduce it.\' \'I don\'t think it\'s at all comfortable, and it said in a voice she had tired herself out with his head!\' she said, by way of speaking to a lobster--\' (Alice began to say to itself in a voice outside, and stopped to listen. \'Mary Ann! Mary Ann!\' said the.', '9460d2be3f2a570036452f95a6187a1d.jpg', '2017-09-17 07:16:05', '2017-09-17 07:16:05'),
(23, 5, 2, 'Aliquam tempora perspiciatis saepe aut iure.', 'Allow me to introduce some other subject of conversation. While she was losing her temper. \'Are you content now?\' said the March Hare said to herself. (Alice had no reason to be full of tears, but said nothing. \'Perhaps it doesn\'t matter a bit,\' said the Pigeon; \'but I haven\'t been invited yet.\' \'You\'ll see me there,\' said the Cat again, sitting on a branch of a book,\' thought Alice \'without.', '354cb448ccb4d3cf407d7e36517b173c.jpg', '2017-09-17 07:16:07', '2017-09-17 07:16:07'),
(24, 5, 5, 'Optio exercitationem assumenda fugit officiis voluptatum.', 'Alice. \'But you\'re so easily offended!\' \'You\'ll get used to do:-- \'How doth the little--\"\' and she felt a little house in it about four inches deep and reaching half down the chimney!\' \'Oh! So Bill\'s got the other--Bill! fetch it back!\' \'And who are THESE?\' said the Duchess, \'as pigs have to go and get ready for your walk!\" \"Coming in a very curious thing, and longed to get dry again: they had.', 'f1c7418f6bfd66584a48e535bb377a09.jpg', '2017-09-17 07:16:08', '2017-09-17 07:16:08'),
(25, 5, 3, 'Iure ea officia quod dolore omnis sit.', 'I am, sir,\' said Alice; not that she was walking by the White Rabbit, trotting slowly back again, and all that,\' he said to the little passage: and THEN--she found herself lying on their slates, and she heard a little way forwards each time and a Dodo, a Lory and an old woman--but then--always to have him with them,\' the Mock Turtle, \'Drive on, old fellow! Don\'t be all day to day.\' This was not.', 'f0e87d374d55d08186ad1dc2921f307a.jpg', '2017-09-17 07:16:10', '2017-09-17 07:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `picture`, `cover_photo`, `created_at`, `updated_at`) VALUES
(1, 1, 'bee36b5152c71325e3c5c9749afaca9c.jpg', 'a8dc4242ebc6ed7c3ea82f47d833a967.jpg', '2017-09-17 07:15:15', '2017-09-17 07:15:15'),
(2, 2, 'efdf4664871a793b01d7123fc6426470.jpg', 'ae1a40aec5f0bda4eaa96ed2630c96c2.jpg', '2017-09-17 07:15:18', '2017-09-17 07:15:18'),
(3, 3, '141b5ce722ae58720246ba2356324fd7.jpg', '487f9ac40628b3be22c8f10e04bd32e1.jpg', '2017-09-17 07:15:22', '2017-09-17 07:15:22'),
(4, 4, '2b3960e53c02636df993735adecf206a.jpg', '157a722f2e75ab6733ea325dba7c057e.jpg', '2017-09-17 07:15:25', '2017-09-17 07:15:25'),
(5, 5, '31736213d710d8327829c9e3b91e9ce0.jpg', '7b0b339b93bff70691090625db21708c.jpg', '2017-09-17 07:15:28', '2017-09-17 07:15:28'),
(6, 6, '2017-09-17-13-22-22-Engrnabi.jpg', '2017-09-17-13-22-22-Engrnabi.jpg', '2017-09-17 07:22:22', '2017-09-17 07:22:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dwight Hessel', 'bsatterfield@example.net', '$2y$10$det76mJCURtUu5O/NIX0XuUg0Ba18PLXc7DwvO.ySr0TE.lrtLqfG', 'sa8mgrLOIr', '2017-09-17 07:15:12', '2017-09-17 07:15:12'),
(2, 'Dr. Delia Bernier PhD', 'ybartoletti@example.org', '$2y$10$det76mJCURtUu5O/NIX0XuUg0Ba18PLXc7DwvO.ySr0TE.lrtLqfG', 'LMp2r25O8E', '2017-09-17 07:15:12', '2017-09-17 07:15:12'),
(3, 'Santina Olson V', 'mitchell.ollie@example.com', '$2y$10$det76mJCURtUu5O/NIX0XuUg0Ba18PLXc7DwvO.ySr0TE.lrtLqfG', '88lCPYLkwO', '2017-09-17 07:15:12', '2017-09-17 07:15:12'),
(4, 'Jayda Douglas', 'kuphal.axel@example.org', '$2y$10$det76mJCURtUu5O/NIX0XuUg0Ba18PLXc7DwvO.ySr0TE.lrtLqfG', 'I6yPKwWWi6', '2017-09-17 07:15:12', '2017-09-17 07:15:12'),
(5, 'Summer Bauch', 'purdy.esperanza@example.com', '$2y$10$det76mJCURtUu5O/NIX0XuUg0Ba18PLXc7DwvO.ySr0TE.lrtLqfG', 'YQDHMwDqcs', '2017-09-17 07:15:12', '2017-09-17 07:15:12'),
(6, 'Engrnabi', 'engrnabi@gmail.com', '$2y$10$ObrgZx.IdeVhK8MQp1ldX.tjw89Lx5SKy7aTLIrjj6t8oxVvJgngy', 'nyW2fm1oXtCx00RVmw8Q5r7GgSLsxOx2QQBBEq76aEAJkpFQFSl2k4FhDFGA', '2017-09-17 07:22:22', '2017-09-17 07:22:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_image_unique` (`image`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
