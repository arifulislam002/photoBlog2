<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('title');
            $table->text('body')->nullable();
            $table->string('image')->unique()->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $directories = ['post-image','post-banner'];
        foreach ($directories as $directory){
            $images = glob(public_path('uploads/'.$directory.'/') .  "*.{jpg,jpeg,png,gif}", GLOB_BRACE);
            foreach($images as $file) {
                if(is_file($file)) {
                    @unlink($file);
                }
            }
        }

        Schema::dropIfExists('posts');
    }
}
