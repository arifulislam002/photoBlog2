<?php
$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    $placeholders = ['people', 'nature'];
    $img_type = $faker->randomElement($placeholders);
    return [
        'picture'=>$faker->unique()->image(public_path('uploads/user-picture'), 265, 265, $img_type, false),
        'cover_photo'=>$faker->unique()->image(public_path('uploads/user-cover-photo'), 1345, 374, $img_type, false)
    ];
});