<?php

use Carbon\Carbon;
$factory->define(App\Post::class, function (Faker\Generator $faker) {

    $placeholders = ['abstract', 'animals', 'business', 'cats', 'city', 'food',
        'nightlife', 'fashion', 'people', 'nature', 'sports', 'technics', 'transport'];

    $img_type = $faker->randomElement($placeholders);

    return [
            'title' => $faker->unique()->sentence,
            'category_id' => $faker->randomElement(App\Category::pluck('id')->toArray()),
            'body' => $faker->unique()->realText(400),
            'image' => $faker->unique()->image(public_path('uploads/post-image'), 500, 325, $img_type, false),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ];
});

